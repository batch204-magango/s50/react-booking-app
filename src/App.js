import { useState} from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';//the as keyword gives an alias to the component upon import
import './App.css';
import AppNavBar from './components/AppNavBar';
import Courses  from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import { UserProvider } from './UserContext';


// import CourseCard from './components/CourseCard';
/*
All other components/pages will be contained in our main component: <App />

<>..</> = Fragment which ensures that adjacent JSX elements will be rendered and avoid this error.

*/


export default function App() {


  const [user, setUser] = useState({email:localStorage.getItem("email")})


  return (

    <UserProvider value={{user, setUser}}>
  <Router>
  <>
  <AppNavBar />
  <Container>
  <Switch>
    <Route  exact path ="/"component = {Home} />
    <Route  exact path="/courses"component = {Courses} />
    <Route exact path="/register"component = {Register}/>
    <Route exact path="/login"component = {Login}/>
    <Route exact path="/logout"component = {Logout}/>
    <Route component={Error}/>
  </Switch>
  </Container>
  </>
  </Router>
    </UserProvider>


);
}
