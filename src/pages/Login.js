import { useState, useEffect, useContext} from 'react';
import { Form, Button} from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import  UserContext  from '../UserContext';

export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);

	const { user, setUser} = useContext(UserContext);

	

	/*
	To properly change and save input valuess, we must implement two -way binding
	We need to capture whatever the user types in the input as they are typing
	Meaning we need the input's .value value
	To get the value, we capture then event (in this case, onChange). The target of the onChange event is the input,meaning we can get the .value

	*/

	useEffect(() => {
		// console.log(email)
		// console.log(password1)
		// console.log(password2)
		if((email !== '' && password !== '' ) ) {
			setIsActive(true)
			}else{
			setIsActive(false)
			
		}
	}, [email, password])

	function loginUser(e){
		e.preventDefault()//prevent default form behavior, so that the form does not submit

		/*activity s55  API Integration with Fetch
			Create a fetch request inside this function to allow users to login.
			log in the console the response
		*/
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
		});


		//localStorage.setItem allows
	// 	localStorage.setItem('email', email)

	// 	setUser({
	// 		email: email
	// 	})


	// 	setEmail("")
	// 	setPassword("")
	
	// 	alert('Thank you for logging in')
	 }

	return(

		<>
		{(user.email !== null) ?
			<Redirect to="/"/>
		  	:
		  	<>

		<Form onSubmit ={e => loginUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value ={email}
					onChange ={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
				
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					value ={password}
					onChange ={e => setPassword(e.target.value)}
					required
				/>
				
			</Form.Group>

			

			{isActive ?
				<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
				Submit
			</Button>
			:
			<Button className="mt-3" variant="primary"id="submitBtn" disabled>
				Login
			</Button>
		}

	
		
			


			
		</Form>
	</>
		}	

		</>	
	)


		
}